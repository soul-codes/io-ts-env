import { mapLeft } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/pipeable";
import { DecodeError, Key } from "io-ts/lib/DecodeError";
import { Decoder } from "io-ts/lib/Decoder";
import { FreeSemigroup, Of, concat, fold, of } from "io-ts/lib/FreeSemigroup";

import { casedPhraseBuilder } from "./casedPhaseBuilder";
import { casedPhaseParser } from "./casedPhraseParser";
import { DefaultOptions } from "./DefaultOptions";
import { EnvDecoderOptions } from "./EnvDecoderOptions";

/**
 * Creates a decoder combinator that transforms a decoder expecting a possibly
 * nested object structure with conventional casing into a decoder expecting
 * a flat object structure with env-style upper-snake-case properties.
 * @param options
 */
export function forEnv(options: EnvDecoderOptions = {}) {
  const filledOptions: Required<EnvDecoderOptions> = {
    ...DefaultOptions,
    ...options,
  };

  return <A>(decoder: Decoder<unknown, A>): Decoder<unknown, A> => {
    const decode: Decoder<unknown, A>["decode"] = (input) => {
      const envDecoded =
        typeof input === "object" && input
          ? parseEnvAsObjectStructure(input)
          : input;
      return pipe(decoder.decode(envDecoded), mapLeft(mapErrors));
    };
    return { decode };
  };

  function parseEnvAsObjectStructure(obj: any) {
    const result: any = Object.create(null);
    const envKeys = Object.keys(obj).sort();
    for (const envKey of envKeys) {
      const propertyPath = envKey
        .split(filledOptions.objectSeparator)
        .filter(Boolean);
      if (!propertyPath.length) continue;

      const leafProperty = propertyPath[propertyPath.length - 1];
      const branchProperties = propertyPath.slice(0, propertyPath.length - 1);
      let base = result;
      for (const property of branchProperties) {
        const casedPropertyName = getCasedPropertyName(property);
        base =
          typeof base[casedPropertyName] === "object" && base[casedPropertyName]
            ? base[casedPropertyName]
            : (base[casedPropertyName] = Object.create(null));
      }
      const propName = getCasedPropertyName(leafProperty);
      if (propName) base[propName] = obj[envKey];
    }
    return result;
  }

  function getCasedPropertyName(fragment: string) {
    return casedPhraseBuilder[filledOptions.wordCasing](
      fragment.split(filledOptions.wordSeparator).filter(Boolean)
    );
  }

  function mapErrors(
    error: FreeSemigroup<DecodeError<string>>,
    keyErrorStack: Key<unknown>[] = []
  ): FreeSemigroup<DecodeError<string>> {
    return fold<DecodeError<string>, FreeSemigroup<DecodeError<string>>>(
      (error) => {
        switch (error._tag) {
          case "Key": {
            const converted = of({
              ...error,
              key: [...keyErrorStack, error]
                .map((error) =>
                  casedPhaseParser[filledOptions.wordCasing](String(error.key))
                    .join(filledOptions.wordSeparator)
                    .toUpperCase()
                )
                .join(filledOptions.objectSeparator),
              errors: mapErrors(error.errors, [...keyErrorStack, error]),
            });
            return collapseNestedKeyError(converted);
          }
          case "Lazy": {
            return of({
              ...error,
              errors: mapErrors(error.errors, keyErrorStack),
            });
          }
          case "Leaf":
          case "Index":
          case "Member":
          case "Wrap":
            return of(error);
        }
      },
      (left, right) =>
        concat(mapErrors(left, keyErrorStack), mapErrors(right, keyErrorStack))
    )(error);
  }

  function collapseNestedKeyError(
    error: FreeSemigroup<DecodeError<string>>
  ): FreeSemigroup<DecodeError<string>> {
    return fold<DecodeError<string>, FreeSemigroup<DecodeError<string>>>(
      (error) => {
        if (error._tag !== "Key") return of(error);
        let lastKeyError = error;
        let base = error.errors;
        while (base._tag === "Of") {
          switch (base.value._tag) {
            case "Key":
              lastKeyError = base.value;
              base = base.value.errors;
              break;
            case "Lazy":
            case "Wrap":
              base = base.value.errors;
              break;
            case "Member":
            case "Leaf":
            case "Index":
              return of(lastKeyError);
          }
        }
        throw Error("Unreachable");
      },
      (left, right) =>
        concat(collapseNestedKeyError(left), collapseNestedKeyError(right))
    )(error);
  }
}
