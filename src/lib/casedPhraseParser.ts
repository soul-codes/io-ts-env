import { WordCasingStyle } from "./WordCasingStyle";

/**
 * Utilities for parsing conventionally cased phrases into arrays of words.
 */
export const casedPhaseParser: Record<
  WordCasingStyle,
  (phrase: string) => string[]
> = {
  lowerCamelCase: (phrase) =>
    phrase
      .split(/(?=\p{Lu})/gu)
      .map((word) => word.toLowerCase())
      .filter(Boolean),
  upperCamelCase: (phrase) =>
    phrase
      .split(/(?=\p{Lu})/gu)
      .map((word) => word.toLowerCase())
      .filter(Boolean),
  lowerSnakeCase: (phrase) =>
    phrase
      .split("_")
      .map((word) => word.toLowerCase())
      .filter(Boolean),
  upperSnakeCase: (phrase) =>
    phrase
      .split("_")
      .map((word) => word.toLowerCase())
      .filter(Boolean),
  lowerKebabCase: (phrase) =>
    phrase
      .split("-")
      .map((word) => word.toLowerCase())
      .filter(Boolean),
  upperKebabCase: (phrase) =>
    phrase
      .split("-")
      .map((word) => word.toLowerCase())
      .filter(Boolean),
};
