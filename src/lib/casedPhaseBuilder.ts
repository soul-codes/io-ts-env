import { WordCasingStyle } from "./WordCasingStyle";

/**
 * Utilities for making conventionally cased phrases from arrays of words.
 */
export const casedPhraseBuilder: Record<
  WordCasingStyle,
  (words: string[]) => string
> = {
  lowerCamelCase: ([first, ...rest]) =>
    first == null
      ? ""
      : [
          first.toLowerCase(),
          ...rest.map(
            ([first, ...rest]) =>
              first.toUpperCase() + rest.join("").toLowerCase()
          ),
        ].join(""),
  upperCamelCase: (words) =>
    words
      .map(
        ([first, ...rest]) => first.toUpperCase() + rest.join("").toLowerCase()
      )
      .join(""),
  lowerSnakeCase: (words) => words.map((word) => word.toLowerCase()).join("_"),
  upperSnakeCase: (words) => words.map((word) => word.toUpperCase()).join("_"),
  lowerKebabCase: (words) => words.map((word) => word.toLowerCase()).join("-"),
  upperKebabCase: (words) => words.map((word) => word.toUpperCase()).join("-"),
};
