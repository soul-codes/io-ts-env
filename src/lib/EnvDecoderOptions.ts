import { WordCasingStyle } from "./WordCasingStyle";

export interface EnvDecoderOptions {
  /**
   * Defines how properties from nested objects are jointed together in the
   * flattened environment variable style. Defaults to `__` (double underscore).
   */
  objectSeparator?: string;

  /**
   * Defines how conventionally-cased object properties (camel-, kebab- or
   * snake-casing) are separated once they are transformed to environment variable
   * style upper case. Defaults to `_` which generates upper-snake-case variable
   * names.
   */
  wordSeparator?: string;

  /**
   * Defines how the multiword properties from the environment variable are
   * cased after they are parsed by the word separator option and before being
   * passed ot the original decoder. Defaults to `lowerCamelCase`.
   */
  wordCasing?: WordCasingStyle;
}
